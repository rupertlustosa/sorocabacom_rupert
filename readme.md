<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Desenvolvedor

Rupert Brasil Lustosa  (rupertlustosa@gmail.com)

Teresina - Pi

## Recursos utilizados

- Checagem de tipo com: declare(strict_types=1);
- Separação da Lógica na camada de Serviços (namespace App\Services)
- Cache - A aplicação faz uso de cache para carregar os cards para a área pública (Vide app/Services/CardService.php). Dependendo da demanda da aplicação, poderia ser utilizada o Redis ou Mencached utilizando-se do recurso de Tags de cache do Laravel. Infelizmente esse recurso não está disponível para os caches de Arquivo, Array ou Banco de dados 
- Permissões básicas utilizando as Policies (vide ThemePolicy método create)
- Nos serviçes (App\Services\*) temos exemplos de uso de Transação  

## Painel

- A URL de acesso é o /panel
- Para logar, temos os usuários projetos@sorocabacom.com.br e rupertlustosa@gmail.com. 
- A senha padrão é 12345678

# API

- Foi desenvolvido apenas um exemplo funcional, com métodos de Login, detalhes do usuário logado, listagem de todos os cards, listagem paginada dos cards e detalhes de um card
- Segue mais informações de USO: https://documenter.getpostman.com/view/1169576/SVYovg8T?version=latest
- Para facilitar os testes, você pode importar a coleção para o Postmam. Ela está em: _data/Sorocaba.postman_collection.json

# TODO

- Documentar a API
