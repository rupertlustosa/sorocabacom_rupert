<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\LoginController@login');

Route::namespace('Api')
    //->middleware(['auth'])
    ->middleware('jwt.auth')
    ->group(function ($api) {

        $api->get('me', function () {

            return auth('api')->user();
        });

        $api->get('cards/all', 'CardController@all')->name('cards.all');
        $api->resource('cards', CardController::class)->only(['index', 'show', 'store', 'update']);
    });
