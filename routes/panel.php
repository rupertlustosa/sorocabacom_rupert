<?php

Route::namespace('Panel')
    ->middleware(['auth'])
    ->prefix('panel')
    ->group(function ($panel) {

        $panel->get('/', 'DashboardController@dashboard')->name("dashboard");

        /* panel/cards */
        $panel->resource('cards', CardController::class);

        /* panel/themes */
        $panel->resource('themes', ThemeController::class);

        /* panel/settings */
        $panel->resource('settings', SettingController::class);

        /* panel/contacts */
        $panel->resource('contacts', ContactController::class);

        /*
         * Rotas de imagens
         * */
        $panel->get('cards/crop/{id}/{setting}', 'CardController@imageCrop')->name('cards.imageCrop');
        $panel->put('cards/update-crop/{id}/{setting}', 'CardController@updateImageCrop')->name('cards.updateImageCrop');
        $panel->get('themes/crop/{id}/{setting}/{imageOrder}', 'ThemeController@imageCrop')->name('themes.imageCrop');
        $panel->put('themes/update-crop/{id}/{setting}', 'ThemeController@updateImageCrop')->name('themes.updateImageCrop');

        # rotas para panel
    });
