<?php

Route::prefix('images')
    ->group(function ($image) {

        // Imagem salva no banco de dados
        $image->get('{folder}/{filename}', function ($folder, $filename) {

            $path = 'images/' . $folder . '/' . $filename;
            $file = Storage::get($path);
            return Image::make($file)->response();
        });

        $image->get('{width}/{folder}/{filename}', function ($width, $folder, $filename) {

            $path = 'images/' . $folder . '/' . $filename;
            $file = Storage::get($path);

            return Image::make($file)->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            })->response();
        })->where('width', '[0-9]+');

        $image->get('original/{width}/{folder}/{filename}', function ($width, $folder, $filename) {

            $path = 'images/' . $folder . '/Original/' . $filename;
            $file = Storage::get($path);

            return Image::make($file)->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            })->response();
        })->where('width', '[0-9]+');
    });
