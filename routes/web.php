<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Front')
    ->name('front.')
    ->group(function ($front) {

        $front->get('/', 'FrontController@index')->name('index');
        $front->post('contact', 'FrontController@storeContact')->name('store_contact');
    });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
