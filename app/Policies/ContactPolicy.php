<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       02/08/2019 09:25:02
 */

declare(strict_types=1);

namespace App\Policies;

use App\Models\Contact;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any contact.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {

        return true;
    }

    /**
     * Determine whether the user can create contact.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {

        return false;
    }

    /**
     * Determine whether the user can update the contact.
     *
     * @param User $user
     * @param Contact $contact
     * @return mixed
     */
    public function update(User $user, Contact $contact)
    {

        return true;
    }

    /**
     * Determine whether the user can delete the contact.
     *
     * @param User $user
     * @param Contact $contact
     * @return mixed
     */
    public function delete(User $user, Contact $contact)
    {

        return true;
    }
}
