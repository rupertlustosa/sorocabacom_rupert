<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 09:09:06
 */

declare(strict_types=1);

namespace App\Policies;

use App\Models\Theme;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThemePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any theme.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {

        return true;
    }

    /**
     * Determine whether the user can create theme.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {

        return !Theme::count();
    }

    /**
     * Determine whether the user can update the theme.
     *
     * @param User $user
     * @param Theme $theme
     * @return mixed
     */
    public function update(User $user, Theme $theme)
    {

        return true;
    }

    /**
     * Determine whether the user can delete the theme.
     *
     * @param User $user
     * @param Theme $theme
     * @return mixed
     */
    public function delete(User $user, Theme $theme)
    {

        return true;
    }
}
