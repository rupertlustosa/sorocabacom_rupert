<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 10:30:40
 */

declare(strict_types=1);

namespace App\Policies;

use App\Models\Card;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CardPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->is_dev) {

            return true;
        }
    }

    /**
     * Determine whether the user can view any card.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {

        return true;
    }

    /**
     * Determine whether the user can create card.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {

        return true;
    }

    /**
     * Determine whether the user can update the card.
     *
     * @param User $user
     * @param Card $card
     * @return mixed
     */
    public function update(User $user, Card $card)
    {

        return true;
    }

    /**
     * Determine whether the user can delete the card.
     *
     * @param User $user
     * @param Card $card
     * @return mixed
     */
    public function delete(User $user, Card $card)
    {

        return true;
    }
}
