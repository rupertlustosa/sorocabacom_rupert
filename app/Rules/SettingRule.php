<?php
/**
 * @package    Rules
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 10:47:23
 */

declare(strict_types=1);

namespace App\Rules;

class SettingRule
{

    /**
     * Validation rules that apply to the request.
     *
     * @var array
     */
    protected static $rules = [
        'id' => 'required',
        'name' => 'required|min:2|max:255',
        'key' => 'required|min:2|max:255',
        'value' => 'required|min:2|max:500',
    ];

    /**
     * Return default rules
     *
     * @return array
     */
    public static function rules()
    {

        return [
            'name' => self::$rules['name'],
            'key' => self::$rules['key'],
            'value' => self::$rules['value'],
        ];
    }

    /**
     * Return default messages
     *
     * @return array
     */
    public static function messages()
    {

        return [];
    }
}
