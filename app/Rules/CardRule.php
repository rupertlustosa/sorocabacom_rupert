<?php
/**
 * @package    Rules
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 10:30:40
 */

declare(strict_types=1);

namespace App\Rules;

class CardRule
{

    /**
     * Validation rules that apply to the request.
     *
     * @var array
     */
    protected static $rules = [
        'id' => 'required',
        'name' => 'required|min:2|max:255',
        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'imageUpdate' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'head' => 'required|min:2|max:200',
        'content' => 'required',
        'active' => 'required',
    ];

    /**
     * Return default rules
     *
     * @return array
     */
    public static function rules()
    {

        return [
            'name' => self::$rules['name'],
            'image' => self::$rules['image'],
            'imageUpdate' => self::$rules['imageUpdate'],
            'head' => self::$rules['head'],
            'content' => self::$rules['content'],
            'active' => self::$rules['active'],
        ];
    }

    /**
     * Return default messages
     *
     * @return array
     */
    public static function messages()
    {

        return [];
    }
}
