<?php
/**
 * @package    Rules
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       02/08/2019 09:25:02
 */

declare(strict_types=1);

namespace App\Rules;

class ContactRule
{

    /**
     * Validation rules that apply to the request.
     *
     * @var array
     */
	protected static $rules = [
		'id' => 'required',
        'name' => 'required|min:2|max:255',
        'email' => 'required|email',
        'message' => 'required|min:2|max:400',
        'ip' => 'required|min:2|max:255',
	];

    /**
     * Return default rules
     *
     * @return array
     */
    public static function rules()
    {

        return [
            'name' => self::$rules['name'],
            'email' => self::$rules['email'],
            'message' => self::$rules['message'],
            'ip' => self::$rules['ip'],
        ];
    }

    /**
     * Return default messages
     *
     * @return array
     */
    public static function messages()
    {

        return [];
    }
}
