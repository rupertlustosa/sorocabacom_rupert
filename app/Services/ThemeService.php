<?php
/**
 * @package    Services
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 09:09:06
 */

declare(strict_types=1);

namespace App\Services;

use App\Models\Theme;
use App\Rlustosa\GenericImageUpload;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ThemeService
{

    private function buildQuery(): Builder
    {

        $query = Theme::query();

        $query->when(request('id'), function ($query, $id) {

            return $query->whereId($id);
        });

        $query->when(request('search'), function ($query, $search) {

            return $query->where('id', 'LIKE', '%' . $search . '%');
        });

        return $query;
    }

    public function paginate(int $limit): LengthAwarePaginator
    {

        return $this->buildQuery()->paginate($limit);
    }

    public function all(): Collection
    {

        return $this->buildQuery()->get();
    }

    public function find(int $id): ?Theme
    {

        return Theme::find($id);
    }

    public function create(array $data): Theme
    {

        // Recuperando as imagens
        foreach ([1, 2] as $field) {

            $image = $this->upload('image' . $field);

            if ($image !== null) {

                $data['image' . $field] = $image;
            } elseif (request('delete_image_' . $field)) {

                $data['image' . $field] = null;
            }
        }

        return DB::transaction(function () use ($data) {

            $model = new Theme();
            $model->fill($data);
            $model->save();

            return $model;
        });
    }

    public function update(array $data, Theme $model): Theme
    {

        // Recuperando as imagens
        foreach ([1, 2] as $field) {

            $image = $this->upload('image' . $field);

            if ($image !== null) {

                $data['image' . $field] = $image;
            } elseif (request('delete_image_' . $field)) {

                $data['image' . $field] = null;
            }
        }

        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete(Theme $model): ?bool
    {

        $model->save();

        return $model->delete();
    }

    public function lists(): array
    {

        return Theme::orderBy('name')
            ->pluck('name', 'id')
            ->toArray();
    }


    private function upload(string $field): ?string
    {

        if (request()->file($field)) {

            return GenericImageUpload::store(request()->file($field), 'themes_' . $field);
        } else {

            return null;
        }
    }

    public function themeToFront(): ?Theme
    {

        return Cache::remember('theme', config('cache.cache_time'), function () {

            return Theme::first();
        });
    }
}
