<?php
/**
 * @package    Services
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 10:30:40
 */

declare(strict_types=1);

namespace App\Services;

use App\Models\Card;
use App\Rlustosa\GenericImageUpload;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CardService
{

    private function buildQuery(): Builder
    {

        $query = Card::query();

        $query->when(request('id'), function ($query, $id) {

            return $query->whereId($id);
        });

        $query->when(request('search'), function ($query, $search) {

            return $query->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('head', 'LIKE', '%' . $search . '%');
            });
        });

        return $query;
    }

    public function paginate(int $limit): LengthAwarePaginator
    {

        return $this->buildQuery()->paginate($limit);
    }

    public function all(): Collection
    {

        return $this->buildQuery()->get();
    }

    public function find(int $id): ?Card
    {

        return Card::find($id);
    }

    public function create(array $data): Card
    {

        $data['image'] = $this->upload();

        $model = new Card();
        $model->fill($data);
        $model->save();

        Cache::flush('cards');
        return $model;
    }

    public function update(array $data, Card $model): Card
    {

        $image = $this->upload();

        if ($image !== null) {

            $data['image'] = $image;
        } elseif (request('delete_image')) {

            $data['image'] = null;
        }
        $model->fill($data);
        $model->save();

        Cache::flush('cards');

        return $model;
    }

    public function delete(Card $model): ?bool
    {

        $return = $model->delete();
        Cache::flush('cards');
        return $return;
    }

    public function lists(): array
    {

        return Card::orderBy('name')
            ->pluck('name', 'id')
            ->toArray();
    }

    private function upload(): ?string
    {

        if (request()->file('image')) {

            return GenericImageUpload::store(request()->file('image'), 'cards');
        } else {

            return null;
        }
    }

    public function cardsToFront(): Collection
    {

        return Cache::remember('cards', config('cache.cache_time'), function () {

            return Card::select('id', 'name', 'image', 'head', 'content')
                ->orderBy('name')
                ->whereActive(1)
                ->get();
        });
    }
}
