<?php
/**
 * @package    Services
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       02/08/2019 09:25:02
 */

declare(strict_types=1);

namespace App\Services;

use App\Models\Contact;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ContactService
{

    private function buildQuery(): Builder
    {

        $query = Contact::query();

        $query->when(request('id'), function ($query, $id) {

            return $query->whereId($id);
        });

        $query->when(request('search'), function ($query, $search) {

            return $query->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('message', 'LIKE', '%' . $search . '%');
            });
        });

        return $query;
    }

    public function paginate(int $limit): LengthAwarePaginator
    {

        return $this->buildQuery()->paginate($limit);
    }

    public function all(): Collection
    {

        return $this->buildQuery()->get();
    }

    public function find(int $id): ?Contact
    {

        return Contact::find($id);
    }

    public function create(array $data): Contact
    {

        return DB::transaction(function () use ($data) {

            $model = new Contact();
            $model->fill($data);
            $model->save();

            return $model;
        });
    }

    public function update(array $data, Contact $model): Contact
    {

        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete(Contact $model): ?bool
    {

        $model->save();

        return $model->delete();
    }

    public function lists(): array
    {

        return Contact::orderBy('name')
            ->pluck('name', 'id')
            ->toArray();
    }
}
