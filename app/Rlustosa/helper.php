<?php

if (! function_exists('isActiveRoute')) {

    function isActiveRoute($route, $output = 'active')
    {
        if (Route::currentRouteName() === $route) {
            return $output;
        }
    }
}
