<?php

namespace App\Rlustosa;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class GenericImageUpload
{
    public static function store($file, $config)
    {

        $folder = config('upload.' . $config . '.pastaUploadImages');
        $fileName = Carbon::now('America/Fortaleza')->format('YmdHis') . '_' .
            Str::random(4) . '_' .
            rand(1000, 9999) . rand(1000, 9999) . rand(1000, 9999) . '_' .
            Str::random(4) . '.' .
            $file->guessClientExtension();

        $file->storeAs(
            'images/' . $folder . '/Original/', $fileName
        );

        $path = $file->storeAs(
            'images/' . $folder, $fileName
        );

        if (config('upload.' . $config . '.width')) {

            $complete_path = storage_path() . '/app/' . $path;
            $img = Image::make($complete_path);
            $img->fit(config('upload.' . $config . '.width'), config('upload.' . $config . '.height'));
            $img->save($complete_path, 75);
        }

        return str_replace('images/', '', $path);
    }
}
