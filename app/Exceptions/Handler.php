<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param \Exception $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $response = [
            'success' => false,
        ];

        if ($exception instanceof UnauthorizedHttpException) {

            $preException = $exception->getPrevious();
            if ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {

                $response['code'] = 'TOKEN_EXPIRED';
                return response()->json($response, Response::HTTP_UNAUTHORIZED);

            } elseif ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {

                $response['code'] = 'TOKEN_INVALID';
                return response()->json($response, Response::HTTP_UNAUTHORIZED);

            } elseif ($preException instanceof \Tymon\JWTAuth\Exceptions\TokenBlacklistedException) {

                $response['code'] = 'TOKEN_BLACKLISTED';
                return response()->json($response, Response::HTTP_UNAUTHORIZED);

            }
        }
        if ($exception->getMessage() === 'Token not provided') {

            $response['code'] = 'MISSING_TOKEN';
            return response()->json($response, Response::HTTP_UNAUTHORIZED);
        }

        return parent::render($request, $exception);
    }
}
