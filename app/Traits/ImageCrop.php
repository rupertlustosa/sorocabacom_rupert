<?php
/**
 * @package    Traits
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 05:40:05
 */

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait ImageCrop
{

    public function imageCrop(int $id, string $setting, int $imageOrder = null)
    {

        $this->log(__METHOD__);

        $route = route($this->cropModule . '.updateImageCrop', [$id, $setting]);
        $item = $this->service->find($id);
        $image = $item->{'image' . $imageOrder};

        $config = config('upload.' . $setting);

        if ($config['width'] === $config['height']) {

            $aspectRatio = '1/1';
        } elseif ($config['width'] >= $config['height']) {

            $aspectRatio = round($config['width'] / $config['height'], 2) . '/1';
        } elseif ($config['width'] <= $config['height']) {

            $aspectRatio = '1/' . round($config['height'] / $config['width'], 2);
        }
        $label = 'Ajuste o posicionamento da imagem';
        return view('panel._layouts.image-crop', compact('image', 'imageOrder', 'aspectRatio', 'label', 'folder'))
            ->with('module', $this->cropModule)
            ->with('url', $route);
    }

    public function updateImageCrop(int $id, string $setting)
    {

        $this->log(__METHOD__);

        $folder = config('upload.' . $setting . '.pastaUploadImages');

        $imagePath = request('image');
        $originalImagePath = 'images/' . str_replace($folder, $folder . '/Original', $imagePath);

        if (Storage::exists($originalImagePath)) {

            $file = Storage::get($originalImagePath);
            $img = Image::make($file);

            $proporcao = $img->width() / config('upload.sizeShowCrop');
            $dataWidth = round(request('dataWidth') * $proporcao);
            $dataHeight = round(request('dataHeight') * $proporcao);
            $dataX = round(request('dataX') * $proporcao);
            $dataY = round(request('dataY') * $proporcao);

            $img->crop($dataWidth, $dataHeight, $dataX, $dataY);
            $img->resize(config('upload.' . $setting . '.width'), config('upload.' . $setting . '.height'), function ($constraint) {

                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $payload = (string)$img->encode();
            Storage::put(
                'images/' . $imagePath,
                $payload
            );

        } else {

            throw new \Exception('FILE_NOT_EXISTS: ' . storage_path($originalImagePath));
        }

        return redirect()->to(\Config::get('upload.' . $setting . '.urlRedirecionamentoAposCrop'))
            ->with('message', 'Imagem atualizada com sucesso!')
            ->with('messageType', 's');
    }
}
