<?php
/**
 * @package    Requests
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 09:09:06
 */

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\ThemeRule;
use Illuminate\Foundation\Http\FormRequest;

class ThemeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return $this->user()->can('update', $this->theme);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = ThemeRule::rules();
        $rules['image1'] = $rules['imageUpdate'];
        $rules['image2'] = $rules['imageUpdate'];
        unset($rules['image'], $rules['imageUpdate']);

        return $rules;
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {

        return ThemeRule::messages();
    }
}
