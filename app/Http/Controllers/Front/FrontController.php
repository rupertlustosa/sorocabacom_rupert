<?php
declare(strict_types=1);

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Services\CardService;
use App\Services\ContactService;
use App\Services\ThemeService;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(ThemeService $themeService, CardService $cardService): View
    {

        $theme = $themeService->themeToFront();

        if ($theme == null) {

            return view('front.theme_not_found');
        }

        $cardsList = $cardService->cardsToFront();

        return view('front.index')->with([
            'theme' => $theme,
            'cards' => $cardsList,
        ]);
    }

    public function storeContact(ContactService $contactService)
    {

        $data = request()->all();
        $data['ip'] = \Request::ip();

        $contactService->create($data);

        return response()->json([], Response::HTTP_OK);
    }
}
