<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{

    public function login()
    {
        // get email and password from request
        $credentials = request(['email', 'password']);
        $response = [];
        // try to auth and get the token using api authentication
        if (!$token = auth('api')->attempt($credentials)) {
            // if the credentials are wrong we send an unauthorized error in json format

            $response['success'] = false;
            $response['code'] = 'UNAUTHORIZED';
            return response()->json($response, Response::HTTP_UNAUTHORIZED);
        }
        /*return response()->json([
            'token' => $token,
            'type' => 'bearer', // you can ommit this
            'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration
        ]);*/

        $response['success'] = true;
        $response['code'] = 'AUTHORIZED';
        $response['data'] = [
            'token' => $token,
            'type' => 'bearer', // you can ommit this
            'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration
        ];
        return response()->json($response, Response::HTTP_OK);
    }
}
