<?php
/**
 * @package    Api
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 10:30:40
 */

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\CardStoreRequest;
use App\Http\Requests\CardUpdateRequest;
use App\Http\Resources\CardCollection;
use App\Http\Resources\CardResource;
use App\Services\CardService;
use Illuminate\Http\JsonResponse;
use Validator;

class CardController extends ApiBaseController
{

    private $cardsService;

    /**
     * Create a new controller instance.
     *
     * @param CardService $cardsService
     */
    public function __construct(CardService $cardsService)
    {

        $this->middleware('jwt.auth');
        $this->cardsService = $cardsService;
    }

    /**
     * Paginate.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {

        try {

            $limit = (int)(request('limit') ?? 20);
            $data = $this->cardsService->paginate($limit);

            return $this->sendPaginate(new CardCollection($data));

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e);

        }
    }

    /**
     * return all.
     *
     * @return JsonResponse
     */
    public function all(): JsonResponse
    {

        try {

            $data = $this->cardsService->all();

            return $this->sendResource(CardResource::collection($data));

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e);
        }
    }

    /**
     * Find detail using id.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {

        try {

            $item = $this->cardsService->find($id);
            if ($item === null) {

                return $this->sendNotFound();
            }

            return $this->sendResource(new CardResource($item));

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e);

        }
    }

    /**
     * Store.
     *
     * @return JsonResponse
     */
    public function store()
    {
        try {

            if (!\Auth::cards()->can('create', Card::class)) {

                return $this->sendUnauthorized();
            }

            $cardsStoreRequest = new CardStoreRequest();
            $validator = Validator::make(request()->all(), $cardsStoreRequest->rules());

            if ($validator->fails()) {

                return $this->sendBadRequest('Validation Error.', $validator->errors()->toArray());
            }

            $item = $this->cardsService->create(request()->all());

            return $this->sendResponse($item->toArray());

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e);

        }
    }

    /**
     * Update.
     *
     * @return JsonResponse
     */
    public function update()
    {
        try {

            $item = $this->cardsService->find($id);
            if ($item === null) {

                return $this->sendNotFound();
            }

            if (!\Auth::cards()->can('update', Card::class)) {

                return $this->sendUnauthorized();
            }

            $cardsUpdateRequest = new CardUpdateRequest();
            $validator = Validator::make(request()->all(), $cardsUpdateRequest->rules());

            if ($validator->fails()) {

                return $this->sendBadRequest('Validation Error.', $validator->errors()->toArray());
            }

            $item = $this->cardsService->update(request()->all(), $item);

            return $this->sendResponse($item->toArray());

        } catch (\Exception $e) {

            return $this->sendError('Server Error.', $e);

        }
    }
}
