<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 10:47:23
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use JsValidator;
use Illuminate\View\View;
use App\Traits\LogActivity;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use App\Services\SettingService;
use App\Http\Requests\SettingStoreRequest;
use App\Http\Requests\SettingUpdateRequest;

class SettingController extends Controller
{
    use LogActivity;

    private $service;
    private $label;

    public function __construct(SettingService $service)
    {

        $this->service = $service;
        $this->label = 'Configurações';
    }

    public function index(): View
    {

        $this->log(__METHOD__);

        $this->authorize('viewAny', Setting::class);

        $data = $this->service->paginate(20);

        return view('panel.settings.index')
            ->with([
                'data' => $data,
                'label' => $this->label,
            ]);
    }

    public function create(): View
    {

        $this->log(__METHOD__);

        $this->authorize('create', Setting::class);

        $validatorRequest = new SettingStoreRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.settings.form')
            ->with([
                'validator' => $validator,
                'label' => $this->label,
            ]);
    }

    public function store(SettingStoreRequest $settingStoreRequest)
    {

        $this->service->create($settingStoreRequest->all());

        return redirect()->route('settings.' . request('routeTo'))
            ->with([
                'message' => 'Criado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function edit(Setting $setting): View
    {

        $this->log(__METHOD__);

        $this->authorize('update', $setting);

        $validatorRequest = new SettingUpdateRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.settings.form')
            ->with([
                'item' => $setting,
                'label' => $this->label,
                'validator' => $validator,
            ]);
    }

    public function update(SettingUpdateRequest $request, Setting $setting): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->service->update($request->all(), $setting);

        return redirect()->route('settings.index')
            ->with([
                'message' => 'Atualizado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function confirmDestroy(Setting $setting): View
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $setting);

        return view('panel.settings.delete')
            ->with([
                'item' => $setting,
                'label' => $this->label,
            ]);
    }

    public function destroy(Setting $setting): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $setting);

        $this->service->delete($setting);

        return redirect()->route('settings.index')
            ->with([
                'message' => 'Removido com sucesso',
                'messageType' => 's',
            ]);
    }

    public function show(Setting $setting): JsonResponse
    {

        $this->log(__METHOD__);
        $this->authorize('update', $setting);

        return response()->json($setting, 200, [], JSON_PRETTY_PRINT);
    }
}
