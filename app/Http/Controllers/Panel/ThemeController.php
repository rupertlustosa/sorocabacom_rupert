<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 09:09:06
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;

use App\Traits\ImageCrop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use JsValidator;
use Illuminate\View\View;
use App\Traits\LogActivity;
use App\Models\Theme;
use App\Http\Controllers\Controller;
use App\Services\ThemeService;
use App\Http\Requests\ThemeStoreRequest;
use App\Http\Requests\ThemeUpdateRequest;

class ThemeController extends Controller
{
    use LogActivity, ImageCrop;

    private $service;
    private $label;

    public function __construct(ThemeService $service)
    {

        $this->service = $service;
        $this->label = 'Tema';
        $this->cropModule = 'themes';
    }

    public function index(): View
    {

        $this->log(__METHOD__);

        $this->authorize('viewAny', Theme::class);

        $data = $this->service->paginate(20);

        return view('panel.themes.index')
            ->with([
                'data' => $data,
                'label' => $this->label,
            ]);
    }

    public function create(): View
    {

        $this->log(__METHOD__);

        $this->authorize('create', Theme::class);

        $validatorRequest = new ThemeStoreRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.themes.form')
            ->with([
                'validator' => $validator,
                'label' => $this->label,
            ]);
    }

    public function store(ThemeStoreRequest $themeStoreRequest)
    {

        $this->service->create($themeStoreRequest->all());

        return redirect()->route('themes.' . request('routeTo'))
            ->with([
                'message' => 'Criado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function edit(Theme $theme): View
    {

        $this->log(__METHOD__);

        $this->authorize('update', $theme);

        $validatorRequest = new ThemeUpdateRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.themes.form')
            ->with([
                'item' => $theme,
                'label' => $this->label,
                'validator' => $validator,
            ]);
    }

    public function update(ThemeUpdateRequest $request, Theme $theme): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->service->update($request->all(), $theme);

        return redirect()->route('themes.index')
            ->with([
                'message' => 'Atualizado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function confirmDestroy(Theme $theme): View
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $theme);

        return view('panel.themes.delete')
            ->with([
                'item' => $theme,
                'label' => $this->label,
            ]);
    }

    public function destroy(Theme $theme): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $theme);

        $this->service->delete($theme);

        return redirect()->route('themes.index')
            ->with([
                'message' => 'Removido com sucesso',
                'messageType' => 's',
            ]);
    }

    public function show(Theme $theme): JsonResponse
    {

        $this->log(__METHOD__);
        $this->authorize('update', $theme);

        return response()->json($theme, 200, [], JSON_PRETTY_PRINT);
    }
}
