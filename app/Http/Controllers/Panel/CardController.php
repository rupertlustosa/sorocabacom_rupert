<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       31/07/2019 10:30:40
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;

use App\Traits\ImageCrop;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use JsValidator;
use Illuminate\View\View;
use App\Traits\LogActivity;
use App\Models\Card;
use App\Http\Controllers\Controller;
use App\Services\CardService;
use App\Http\Requests\CardStoreRequest;
use App\Http\Requests\CardUpdateRequest;

class CardController extends Controller
{
    use LogActivity, ImageCrop;

    private $service;
    private $label;

    public function __construct(CardService $service)
    {

        $this->service = $service;
        $this->label = 'Cartas';
        $this->cropModule = 'cards';
    }

    public function index(): View
    {

        $this->log(__METHOD__);

        $this->authorize('viewAny', Card::class);

        $data = $this->service->paginate(20);

        return view('panel.cards.index')
            ->with([
                'data' => $data,
                'label' => $this->label,
            ]);
    }

    public function create(): View
    {

        $this->log(__METHOD__);

        $this->authorize('create', Card::class);

        $validatorRequest = new CardStoreRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.cards.form')
            ->with([
                'validator' => $validator,
                'label' => $this->label,
            ]);
    }

    public function store(CardStoreRequest $cardStoreRequest)
    {

        $this->service->create($cardStoreRequest->all());

        return redirect()->route('cards.' . request('routeTo'))
            ->with([
                'message' => 'Criado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function edit(Card $card): View
    {

        $this->log(__METHOD__);

        $this->authorize('update', $card);

        $validatorRequest = new CardUpdateRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.cards.form')
            ->with([
                'item' => $card,
                'label' => $this->label,
                'validator' => $validator,
            ]);
    }

    public function update(CardUpdateRequest $request, Card $card): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->service->update($request->all(), $card);

        return redirect()->route('cards.index')
            ->with([
                'message' => 'Atualizado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function confirmDestroy(Card $card): View
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $card);

        return view('panel.cards.delete')
            ->with([
                'item' => $card,
                'label' => $this->label,
            ]);
    }

    public function destroy(Card $card): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $card);

        $this->service->delete($card);

        return redirect()->route('cards.index')
            ->with([
                'message' => 'Removido com sucesso',
                'messageType' => 's',
            ]);
    }

    public function show(Card $card): JsonResponse
    {

        $this->log(__METHOD__);
        $this->authorize('update', $card);

        return response()->json($card, 200, [], JSON_PRETTY_PRINT);
    }
}
