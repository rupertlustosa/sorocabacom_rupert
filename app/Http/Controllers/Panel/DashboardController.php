<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DashboardController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function dashboard()
    {

        return view('panel.home.dashboard', compact('genericNumbers'));
    }
}
