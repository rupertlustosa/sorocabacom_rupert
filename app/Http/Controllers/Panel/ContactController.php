<?php
/**
 * @package    Controller
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       02/08/2019 09:25:02
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use JsValidator;
use Illuminate\View\View;
use App\Traits\LogActivity;
use App\Models\Contact;
use App\Http\Controllers\Controller;
use App\Services\ContactService;
use App\Http\Requests\ContactStoreRequest;
use App\Http\Requests\ContactUpdateRequest;

class ContactController extends Controller
{
    use LogActivity;

    private $service;
    private $label;

    public function __construct(ContactService $service)
    {

        $this->service = $service;
        $this->label = 'Contatos';
    }

    public function index(): View
    {

        $this->log(__METHOD__);

        $this->authorize('viewAny', Contact::class);

        $data = $this->service->paginate(20);

        return view('panel.contacts.index')
            ->with([
                'data' => $data,
                'label' => $this->label,
            ]);
    }

    public function create(): View
    {

        $this->log(__METHOD__);

        $this->authorize('create', Contact::class);

        $validatorRequest = new ContactStoreRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.contacts.form')
            ->with([
                'validator' => $validator,
                'label' => $this->label,
            ]);
    }

    public function store(ContactStoreRequest $contactStoreRequest)
    {

        $this->service->create($contactStoreRequest->all());

        return redirect()->route('contacts.' . request('routeTo'))
            ->with([
                'message' => 'Criado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function edit(Contact $contact): View
    {

        $this->log(__METHOD__);

        $this->authorize('update', $contact);

        $validatorRequest = new ContactUpdateRequest();
        $validator = JsValidator::make($validatorRequest->rules(), $validatorRequest->messages());

        return view('panel.contacts.form')
            ->with([
                'item' => $contact,
                'label' => $this->label,
                'validator' => $validator,
            ]);
    }

    public function update(ContactUpdateRequest $request, Contact $contact): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->service->update($request->all(), $contact);

        return redirect()->route('contacts.index')
            ->with([
                'message' => 'Atualizado com sucesso',
                'messageType' => 's',
            ]);
    }

    public function confirmDestroy(Contact $contact): View
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $contact);

        return view('panel.contacts.delete')
            ->with([
                'item' => $contact,
                'label' => $this->label,
            ]);
    }

    public function destroy(Contact $contact): RedirectResponse
    {

        $this->log(__METHOD__);

        $this->authorize('delete', $contact);

        $this->service->delete($contact);

        return redirect()->route('contacts.index')
            ->with([
                'message' => 'Removido com sucesso',
                'messageType' => 's',
            ]);
    }

    public function show(Contact $contact): JsonResponse
    {

        $this->log(__METHOD__);
        $this->authorize('update', $contact);

        return response()->json($contact, 200, [], JSON_PRETTY_PRINT);
    }
}
