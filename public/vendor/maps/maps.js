var map;
var idInfoBoxAberto;
var infoBox = [];
var markers = [];

function initialize() {
    var latlng = new google.maps.LatLng(-11.9503158, -63.0382575);

    var options = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("mapa"), options);
}

initialize();

function abrirInfoBox(id, marker) {
    if (typeof (idInfoBoxAberto) == 'number' && typeof (infoBox[idInfoBoxAberto]) == 'object') {
        infoBox[idInfoBoxAberto].close();
    }

    infoBox[id].open(map, marker);
    idInfoBoxAberto = id;
}

function carregarPontos() {

    //$.getJSON('/assets/maps/pontos.json', function(pontos) {
    $.getJSON('/maps/pontos.json', function (pontos) {

        var latlngbounds = new google.maps.LatLngBounds();

        $.each(pontos, function (index, ponto) {

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(ponto.lat, ponto.lng),
                title: "Meu ponto personalizado! :-D",
                icon: '/assets/maps/marcador.png'
            });

            var myOptions = {
                content: "<p>" + ponto.company + "</p>",
                pixelOffset: new google.maps.Size(-150, 0)
            };

            infoBox[ponto.id] = new InfoBox(myOptions);
            infoBox[ponto.id].marker = marker;

            infoBox[ponto.id].listener = google.maps.event.addListener(marker, 'click', function (e) {
                abrirInfoBox(ponto.id, marker);
            });

            markers.push(marker);

            latlngbounds.extend(marker.position);

        });

        var markerCluster = new MarkerClusterer(map, markers);

        map.fitBounds(latlngbounds);

    });

}

carregarPontos();
