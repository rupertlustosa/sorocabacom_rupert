console.info('app/js/custom-scripts.js carregado...');

function block() {

    $.blockUI(
        {
            message: 'Aguarde...',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff',
                'font-size': '16px',
                'font-weight': 'bold'
            }
        }
    );
}

function alerta() {
    alert('Teste de Alerta');
}

function showMessage(t, m, time) {

    if (isNaN(time))
        time = 7;

    time = time * 1000;

    var positionClass;
    if (t != 's' && t != 'i' && t != 'w' && t != 'e') {
        positionClass = 'toast-top-full-width';
        t = 'e';
        m = "MENSAGEM ERRADA";
    } else {
        positionClass = 'toast-top-right';
    }

    setTimeout(function () {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            positionClass: positionClass,
            timeOut: time
        };
        if (t == 's')
            toastr.success(m);
        else if (t == 'i')
            toastr.info(m);
        else if (t == 'w')
            toastr.warning(m);
        else if (t == 'e')
            toastr.error(m);
    }, 0);
}

$(function () {

    $('.mascara_telefone').focusout(function () {
        var phone, element;
        element = $(this);
        phone = element.val().replace(/\D/g, '');
        if (phone.length == 8) {
            element.inputmask('9999-9999');
        } else {
            element.inputmask('99999-9999');
        }
    }).trigger('focusout');

    $('.mascara_telefone_ddd').focusout(function () {
        var phone, element;
        element = $(this);
        phone = element.val().replace(/\D/g, '');
        if (phone.length == 10) {
            element.inputmask({placeholder: '', mask: '(99) 9999-9999'});
        } else {
            element.inputmask({placeholder: '', mask: '(99) 99999-9999'});
        }
    }).trigger('focusout');

    $('.mascara_telefone_ddd_8').inputmask('(99) 9999-9999');
    $('.mascara_telefone_ddd_9').inputmask('(99) 99999-9999');

    $('.mascara_telefone_8').inputmask('9999-9999');
    $('.mascara_telefone_9').inputmask('99999-9999');

    $(".mascara_ddd").inputmask("99");
    $(".mascara_cpf").inputmask("999.999.999-99");
    $(".mascara_cep").inputmask("99999-999");
    $(".mascara_cnpj").inputmask("99.999.999/9999-99");
    $(".mascara_data").inputmask("99/99/9999");
    $(".mascara_data_mes_ano").inputmask("99/9999");

    $(".mascara_peso").maskMoney({
        showSymbol: false,
        decimal: ".",
        thousands: "",
        precision: 2,
        allowZero: true,
        allowNegative: false,
        defaultZero: true
    });
    $(".mascara_moeda").maskMoney({
        symbol: "R$",
        showSymbol: false,
        decimal: ",",
        thousands: ".",
        allowZero: true,
        allowNegative: false,
        defaultZero: true
    })
    $(".mascara_porcentagem").maskMoney({
        showSymbol: false,
        decimal: ".",
        thousands: "",
        precision: 2,
        allowZero: true,
        allowNegative: false,
        defaultZero: true
    });
});

$.fn.select2.defaults.set("theme", "bootstrap");

let placeholder = "Selecione";

$('.select2').select2({
    placeholder: placeholder,
    allowClear: true,
    width: 'resolve'
});

$(".select2-allow-clear").select2({
    allowClear: true,
    placeholder: placeholder,
    containerCssClass: ':all:',
    width: 'resolve'
});
