$(function () {

    console.info('app/js/postal_code.js carregado...');

    $('#postal_code').on('blur', function (e) {
        e.preventDefault();
        let postal_code = $(this).val().replace(/\D/g, '');
        let form = $(this).closest("form");

        if (postal_code != "") {
            block();
            //$('.postal_code-input + .fa').show();
            $.getJSON("//viacep.com.br/ws/" + postal_code + "/json/?callback=?", function (dados) {
                if (!("erro" in dados)) {

                    let address = $(form).find("input[name='address']");
                    if (!$.trim(address.val()).length)
                        address.val(dados.logradouro);

                    let complement = $(form).find("input[name='complement']");
                    if (!$.trim(complement.val()).length)
                        complement.val(dados.complemento);

                    let neighborhood = $(form).find("input[name='neighborhood']");
                    if (!$.trim(neighborhood.val()).length)
                        neighborhood.val(dados.bairro);

                    let city = $(form).find("input[name='city']");
                    if (!$.trim(city.val()).length)
                        city.val(dados.localidade);

                    let state = $(form).find("input[name='state']");
                    if (!$.trim(state.val()).length)
                        state.val(dados.uf);

                    let newOption = new Option((dados.localidade + " - " + dados.uf), dados.ibge, false, false);
                    $('#city_id').append(newOption).trigger('change');

                    $('#city_id').val(dados.ibge);
                    $('#city_id').trigger('change');

                }
            }).always(function () {

                $.unblockUI();
                $('#number').focus();
            });
        }
    });
});
