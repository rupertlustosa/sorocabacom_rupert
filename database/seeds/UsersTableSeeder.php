<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                "id" => 1,
                "is_dev" => true,
                "name" => "Kelly Alves",
                "email" => "projetos@sorocabacom.com.br",
            ),
            array(
                "id" => 2,
                "is_dev" => true,
                "name" => "Rupert Lustosa",
                "email" => "rupertlustosa@gmail.com",
            ),
        );

        $password = \Hash::make('12345678');

        foreach ($users as $item) {

            $item['password'] = $password;
            \App\Models\User::create($item);
        }
    }
}
