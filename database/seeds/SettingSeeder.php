<?php
/**
 * @package    Seeder
 * @author     Rupert Brasil Lustosa <rupertlustosa@gmail.com>
 * @date       01/08/2019 10:47:23
 */

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $settings = array(
            array(
                "id" => 1,
                "name" => "Título da Página",
                "key" => "PAGE_TITLE",
                "value" => "Transistor",
            ),
            array(
                "id" => 2,
                "name" => "Frase do Formulário",
                "key" => "FORM_PHRASE",
                "value" => "Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado.",
            ),
        );

        foreach ($settings as $item) {

            \App\Models\Setting::create($item);
        }
    }
}
