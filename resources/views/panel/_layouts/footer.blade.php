<div class="footer">
    <div class="pull-right">
        {{ date('Y') }}
    </div>
    <div>
        Powered by <a href="mailto:rupertlustosa@gmail.com">Rupert Lustosa</a>
    </div>
</div>
