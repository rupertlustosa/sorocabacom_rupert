<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'Laravel') }} - @yield('_titulo_pagina_') </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{!! asset('app/app.css?v='.microtime()) !!}"/>

    @yield('styles')
    <style>
        .ibox-title {
            min-height: 58px;
        }

        .top-navigation .navbar-brand {
            background: none;
        }

        .ms-choice {
            width: 99% !important;
        }

        .ms-choice > span {
            top: 6px !important;
            left: 15px !important;
        }

        .form-group label {
            font-weight: 600;
        }

        .skin-1 .navbar-static-top {
            background: #3E425B;
        }

        .top-navigation .navbar .nav > li.active {
            background: none;
        }

        .top-navigation .nav > li a:focus, .top-navigation .nav > li a:hover {
            background: #424760;
            color: #fff;
        }

        .skin-1 #page-wrapper {
            background: #FAFAFA;
        }

        /*.skin-1 #page-wrapper {
            background: #f6f6f6;
            background-image: -webkit-radial-gradient(center, ellipse cover, #f6f6f6 20%, #d5d5d5 100%);
            background-image: -o-radial-gradient(center, ellipse cover, #f6f6f6 20%, #d5d5d5 100%);
            background-image: -ms-radial-gradient(center, ellipse cover, #f6f6f6 20%, #d5d5d5 100%);
            background-image: radial-gradient(ellipse at center, #f6f6f6 20%, #d5d5d5 100%);
            -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#f6f6f6, endColorstr=#d5d5d5)";
        }*/
        .form-control, .single-line {
            border: 1px solid rgb(223, 225, 229);
        }

        .form-control, .matrix-read-only {
            border: 1px solid rgb(223, 225, 229);
            background-color: #fafafa;
        }

        #table-matrix input {
            border: 1px solid rgb(223, 225, 229);
            padding: 2px;
            text-align: center;
            width: 80px;
        }

        .select2-container--bootstrap .select2-dropdown {
            z-index: 9999;
        }

        #table-matrix a {
            color: #343a40;;
        }

        #table-matrix .text-favorable {
            color: green !important;
        }

        #table-matrix .text-unfavorable {
            color: red !important;
        }

        .inactive {
            background-color: rgba(250, 250, 250, 0.6);
        }

        .inactive > td {
            color: #999c9e;
            background-color: rgba(250, 250, 250, 0.6);
        }
    </style>
</head>
<body class="top-navigation skin-1">

<!-- Wrapper-->
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <!-- Navigation -->
    @include('panel._layouts.navigation')

    <!-- Main view  -->
        <div id="topo-tela"></div>
    @yield('content')

    <!-- Footer -->
        @include('panel._layouts.footer')
    </div>
    <!-- End page wrapper-->
</div>
<!-- End wrapper-->
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<script src="{{ asset('/app/app.js?v='.microtime()) }}" type="text/javascript"></script>
<script src="{{ asset('app/js/custom-scripts.js?v='.microtime()) }}" type="text/javascript"></script>

@section('scripts')
@show

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $.fn.select2.defaults.set("theme", "bootstrap");
    });
</script>
</body>
</html>
