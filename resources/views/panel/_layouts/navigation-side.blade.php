<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{ \Auth::user()->name }}</strong>
                            </span>
                            <span class="text-muted text-xs block">Meu Perfil <b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{{ route('users.profile') }}">Editar</a></li>
                        <li><a href="{{ url('logout') }}">Sair</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ isActiveRoute('users.index') }}">
                <a href="{{ route('users.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Usuários</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('lessons.index') }}">
                <a href="{{ route('lessons.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Aulas</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('disciplines.index') }}">
                <a href="{{ route('disciplines.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Disciplinas</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('courses.index') }}">
                <a href="{{ route('courses.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Cursos</span>
                </a>
            </li>
            <li class="{{ isActiveRoute('classes.index') }}">
                <a href="{{ route('classes.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Turmas</span>
                </a>
            </li>

            <li class="{{ isActiveRoute('mural.index') }}">
                <a href="{{ route('mural.index') }}"><i class="fa fa-th-large"></i>
                    <span class="nav-label">Mural</span>
                </a>
            </li>

        </ul>

    </div>
</nav>
