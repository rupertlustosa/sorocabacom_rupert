@extends('panel._layouts.panel')

@section('_titulo_pagina_', 'Lista de '.$label)

@section('content')

    @include('panel.cards.nav')

    @php

        $_placeholder_ = "Localize por 'nome ou chamada'";
    @endphp

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">

                    <div class="ibox-title">

                        <h5>@yield('_titulo_pagina_')</h5>

                        <div class="ibox-tools">

                        </div>
                    </div>

                    <div class="ibox-content">

                        <div class="m-b-lg">
                            <form method="get" id="frm_search" action="{{ route('cards.index') }}">
                                @include('panel._assets.basic-search')
                            </form>
                        </div>

                        <div class="table-responsive">

                            @if($data->count())

                                <table class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th style="width: 90px; text-align: center">Ações</th>

                                        <th>Nome / Chamada</th>
                                        <th style="width: 280px">Imagem</th>
                                        <th style="width: 150px">Ativo?</th>
                                        <th class="hidden-xs hidden-sm" style="width: 150px;">Criado em</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($data->count())
                                        @foreach($data as $item)
                                            <tr {!! $item->active == 0 ? 'class="inactive"' : '' !!}>
                                                <td style="text-align: center">
                                                    <div class="btn-group" role="group">
                                                        <button id="btnGroupDrop1" type="button"
                                                                class="btn btn-default dropdown-toggle"
                                                                data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                            Ações
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                            <a class="dropdown-item"
                                                               href="{{ route('cards.edit', [$item->id]) }}">Editar</a>
                                                            <a class="dropdown-item ln_delete"
                                                               data-id="{{ $item->id }}"
                                                               href="{{ route('cards.destroy', [$item->id]) }}">
                                                                Remover
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>

                                                <td>
                                                    {{ $item->name }}<br>
                                                    <em><strong>Chamada:</strong> {{ $item->head }}</em>
                                                </td>

                                                <td class="text-center">
                                                    @if($item->image)
                                                        <a href="{{ asset('images/'.$item->image) }}" target="_blank">
                                                            <img src="{{ asset('images/100/'.$item->image) }}">
                                                        </a>
                                                        <br/>
                                                        <a href="{{ route('cards.imageCrop', [$item->id, 'cards']) }}">
                                                            <i class="fa fa-crop"></i>
                                                            Recortar
                                                        </a>
                                                    @endif
                                                </td>
                                                <td>{{ $item->active == 1 ? 'Sim' : 'Não' }}</td>
                                                <td class="hidden-xs hidden-sm">{{ $item->created_at->format('d/m/Y H:i') }}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <form method="post" id="frm_delete" action="">

                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>

                                @include('panel._assets.paginate')

                            @else
                                <div class="alert alert-danger">
                                    Não temos nada para exibir. Caso tenha realizado uma busca você pode realizar
                                    uma nova com outros termos ou
                                    <a class="alert-link" href="{{ route('cards.index') }}">
                                        limpar sua pesquisa.
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')

@endsection

@section('scripts')
    @include('panel._assets.scripts-index')

    <script type="text/javascript">
        $('.ln_delete').on('click', function (e) {

            e.preventDefault();

            if (confirm('Você tem certeza?')) {

                let element = $(this);
                let id = element.attr('data-id');
                let url = element.attr('href');
                //$('#frm_delete_' + id).submit();
                $('#frm_delete').attr('action', url).submit();
            }
        });
    </script>
@endsection
