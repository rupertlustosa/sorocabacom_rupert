@extends('panel._layouts.panel')

@section('title', 'Minor page')

@section('content')
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">

                <div class="text-center m-t-lg">
                    <h1>
                        Bem vindo ao painel administrativo!
                    </h1>
                    {{--<small>Navegue pelo menu superior para interagir com as funcionalidades.</small>--}}
                </div>

                <div class="row">

                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('scripts')

@endsection
