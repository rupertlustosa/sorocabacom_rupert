@extends('panel._layouts.panel')

@section('_titulo_pagina_', 'Lista de '.$label)

@section('content')

    @include('panel.contacts.nav')

    @php

        //$_placeholder_ = "Localize por ''";
    @endphp

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">

                    <div class="ibox-title">

                        <h5>@yield('_titulo_pagina_')</h5>

                        <div class="ibox-tools">
                            @if(Auth::user()->can('create', \App\Models\Contact::class))
                                <a href="{{ route('contacts.create') }}" class="btn btn-primary {{--btn-xs--}}">
                                    <i class="fa fa-plus"></i> Cadastrar
                                </a>
                            @endif
                        </div>
                    </div>

                    <div class="ibox-content">

                        <div class="m-b-lg">
                            <form method="get" id="frm_search" action="{{ route('contacts.index') }}">
                                @include('panel._assets.basic-search')
                            </form>
                        </div>

                        <div class="table-responsive">

                            @if($data->count())

                                <table class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th style="width: 90px; text-align: center">Ações</th>

                                        <th>Nome</th>
                                        <th>Mensagem</th>
                                        <th class="hidden-xs hidden-sm" style="width: 150px;">Enviado em</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($data->count())
                                        @foreach($data as $item)
                                            <tr>
                                                <td style="text-align: center">
                                                    <a class="btn btn-sm btn-default ln_delete"
                                                       data-id="{{ $item->id }}"
                                                       href="{{ route('contacts.destroy', [$item->id]) }}">
                                                        <i class="fa fa-trash"></i> Remover
                                                    </a>
                                                </td>

                                                <td style="width: 20%">
                                                    {{ $item->name }}<br>
                                                    <em><strong>Email:</strong> {{ $item->email }}</em><br>
                                                    <em><strong>IP:</strong> {{ $item->ip }}</em>
                                                </td>
                                                <td>{{ $item->message }}</td>
                                                <td class="hidden-xs hidden-sm">{{ $item->created_at->format('d/m/Y H:i') }}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                <form method="post" id="frm_delete" action="">

                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                </form>

                                @include('panel._assets.paginate')

                            @else
                                <div class="alert alert-danger">
                                    Não temos nada para exibir. Caso tenha realizado uma busca você pode realizar
                                    uma nova com outros termos ou
                                    <a class="alert-link" href="{{ route('contacts.index') }}">
                                        limpar sua pesquisa.
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')

@endsection

@section('scripts')
    @include('panel._assets.scripts-index')

    <script type="text/javascript">
        $('.ln_delete').on('click', function (e) {

            e.preventDefault();

            if (confirm('Você tem certeza?')) {

                let element = $(this);
                let id = element.attr('data-id');
                let url = element.attr('href');
                $('#frm_delete').attr('action', url).submit();
            }
        });
    </script>
@endsection
