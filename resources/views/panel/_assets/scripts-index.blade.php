<script>
    $().ready(function () {
        @if (Session::has('message'))
        showMessage('{{ session('messageType') }}', '{{ session('message') }}');
        @endif
    });
</script>
