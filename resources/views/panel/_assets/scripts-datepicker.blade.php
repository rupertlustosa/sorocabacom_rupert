<!-- Data picker -->
<link href="{{ asset('vendor/datapicker/datepicker3.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/datapicker/bootstrap-datepicker.js')}}"></script>

<script>

    $(function () {
        $('.date_calendar .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: true,
            calendarWeeks: true,
            autoclose: true,
            format: "dd/mm/yyyy"
        });
    });
</script>
