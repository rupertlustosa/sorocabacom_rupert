<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{{ asset('vendor/multiple-select/multiple-select.css') }}">

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('vendor/multiple-select/multiple-select.js') }}"></script>

<script>
    $('.multiple-select').multipleSelect()
    /*$('.multiple-select').multipleSelect({
        filter: true
    });*/
</script>
