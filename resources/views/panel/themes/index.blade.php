@extends('panel._layouts.panel')

@section('_titulo_pagina_', 'Lista de '.$label)

@section('content')

    @include('panel.themes.nav')

    @php

        //$_placeholder_ = "Localize por ''";
    @endphp

    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">

                <div class="ibox">

                    <div class="ibox-title">

                        <h5>@yield('_titulo_pagina_')</h5>
                    </div>

                    <div class="ibox-content">

                        <div class="table-responsive">

                            @if($data->count())

                                <table class="table table-striped table-bordered table-hover">

                                    <thead>
                                    <tr>
                                        <th style="width: 90px; text-align: center">Ações</th>

                                        <th>Nome</th>
                                        <th>Imagem de Background</th>
                                        <th>Imagem de Destaque</th>
                                        <th>Chamada</th>
                                        <th class="hidden-xs hidden-sm" style="width: 150px;">Criado em</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if($data->count())
                                        @foreach($data as $item)
                                            <tr>
                                                <td style="text-align: center">
                                                    <a class="btn btn-sm btn-default"
                                                       href="{{ route('themes.edit', [$item->id]) }}">
                                                        <i class="fa fa-pencil"></i> Editar
                                                    </a>
                                                </td>

                                                <td>{{ $item->name }}</td>

                                                <td class="text-center">
                                                    @if($item->image1)
                                                        <a href="{{ asset('images/'.$item->image1) }}" target="_blank">
                                                            <img src="{{ asset('images/100/'.$item->image1) }}">
                                                        </a>
                                                        <br/>
                                                        <a href="{{ route('themes.imageCrop', [$item->id, 'themes_image1', 1]) }}">
                                                            <i class="fa fa-crop"></i>
                                                            Recortar
                                                        </a>
                                                    @endif
                                                </td>

                                                <td class="text-center">
                                                    @if($item->image2)
                                                        <a href="{{ asset('images/'.$item->image2) }}" target="_blank">
                                                            <img src="{{ asset('images/100/'.$item->image2) }}">
                                                        </a>
                                                        <br/>
                                                        <a href="{{ route('themes.imageCrop', [$item->id, 'themes_image2', 2]) }}">
                                                            <i class="fa fa-crop"></i>
                                                            Recortar
                                                        </a>
                                                    @endif
                                                </td>

                                                <td>{{ $item->head }}</td>
                                                <td class="hidden-xs hidden-sm">{{ $item->created_at->format('d/m/Y H:i') }}</td>

                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                                @include('panel._assets.paginate')

                            @else
                                <div class="alert alert-danger">
                                    Não temos nada para exibir. Caso tenha realizado uma busca você pode realizar
                                    uma nova com outros termos ou
                                    <a class="alert-link" href="{{ route('themes.index') }}">
                                        limpar sua pesquisa.
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')

@endsection

@section('scripts')
    @include('panel._assets.scripts-index')
@endsection
