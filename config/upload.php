<?php

return [

    'sizeShowCrop' => 600,

    'cards' => [
        'pastaUploadImages' => 'cards',
        'urlRedirecionamentoAposCrop' => '/panel/cards',
        'width' => 385,
        'height' => 533,
    ],
    'themes_image1' => [
        'pastaUploadImages' => 'themes',
        'urlRedirecionamentoAposCrop' => '/panel/themes',
        'width' => 1920,
        'height' => 1080,
    ],
    'themes_image2' => [
        'pastaUploadImages' => 'themes',
        'urlRedirecionamentoAposCrop' => '/panel/themes',
        'width' => 526,
        'height' => 754,
    ],
];
